using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    #region Declaraciones
    float rapidezDesplazamiento = 5.0f;
    public Camera camaraJugador;
    ControlLuz scriptLinterna;
    public GameObject script;
    public bool instanciarEnemigo;
    GameManagerScript scriptManager;
    public Transform manager;
    bool hActivo;
    bool vActivo;
    bool estoyCaminando;
    #endregion

    void Start()
    {
        scriptLinterna = (ControlLuz)script.GetComponent(typeof(ControlLuz));
        instanciarEnemigo = false;
        scriptManager = (GameManagerScript)manager.GetComponent(typeof(GameManagerScript));
    }

    
    void Update()
    {
        float movimientoAdelanteYAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        
        movimientoAdelanteYAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteYAtras);

        

        if (Input.GetKeyDown(KeyCode.LeftShift) && estoyCaminando)
        {
            rapidezDesplazamiento = 15.0f;
            AkSoundEngine.PostEvent("Play_correr", gameObject);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = 5.0f;
            AkSoundEngine.PostEvent("Stop_correr", gameObject);
        }

        if (scriptLinterna.linternaPrendida == true)
        {
            Ray ray = camaraJugador.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if(Physics.Raycast(ray,out hit) == true && hit.distance <=5)
            {
                //Debug.Log("El rayo esta tocando al objeto: " + hit.collider.name); //Recordar comentar esta linea para la build

                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject enemigo = GameObject.Find(hit.transform.name);
                    ControlEnemigo scriptEnemigo = (ControlEnemigo)enemigo.GetComponent(typeof(ControlEnemigo));

                    if (scriptEnemigo != null)
                    {
                        scriptEnemigo.Detenerse();
                    }
                }
                else
                {
                    if (scriptManager.enemigoInstanciado == true)
                    {
                        GameObject enemigo = GameObject.Find("Bot(Clone)");
                        ControlEnemigo scriptEnemigo = (ControlEnemigo)enemigo.GetComponent(typeof(ControlEnemigo));

                        if (scriptEnemigo != null)
                        {
                            scriptEnemigo.Avanzar();
                        }
                    }
                }
            }
        }
        else if (scriptLinterna.linternaPrendida == false && scriptManager.enemigoInstanciado)
        {
            GameObject enemigo = GameObject.Find("Bot(Clone)");
            ControlEnemigo scriptEnemigo = (ControlEnemigo)enemigo.GetComponent(typeof(ControlEnemigo));

            if (scriptEnemigo != null)
            {
                scriptEnemigo.Avanzar();
            }
        }

        if (Input.GetButtonDown("Horizontal"))
        {
            if (vActivo == false)
            {
                hActivo = true;
                estoyCaminando = true;
                AkSoundEngine.PostEvent("Play_caminar", gameObject);
            }
        }

        if (Input.GetButtonDown("Vertical"))
        {
            if (hActivo == false)
            {
                vActivo = true;
                estoyCaminando = true;
                AkSoundEngine.PostEvent("Play_caminar", gameObject);
            }
        }

        if (Input.GetButtonUp("Horizontal"))
        {
            hActivo = false;
            estoyCaminando = false;
            AkSoundEngine.PostEvent("Stop_caminar", gameObject);
        }

        if (Input.GetButtonUp("Vertical"))
        {
            vActivo = false;
            estoyCaminando = false;
            AkSoundEngine.PostEvent("Stop_caminar", gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("trigger") && instanciarEnemigo == false)
        {
            instanciarEnemigo = true;
            AkSoundEngine.PostEvent("Stop_puerta_abierta", GameObject.Find("Game Manager"));
        }
    }
}
