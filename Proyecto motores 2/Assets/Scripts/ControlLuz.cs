using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLuz : MonoBehaviour
{
    #region Declaraciones
    public Light luzLinterna;
    bool tengoBateria;
    [HideInInspector] public bool linternaPrendida;
    float bateria;
    IEnumerator temporizadorBateria;
    public TMPro.TMP_Text textoBateria;
    #endregion

    void Start()
    {
        tengoBateria = true;
        temporizadorBateria = DescontarBateria(100);
        PrenderLinterna();
    }

    void Update()
    {
        if (luzLinterna.intensity != 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ApagarLinterna();
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && tengoBateria)
            {
                PrenderLinterna();
            }
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            bateria = 0;
        }

        if (tengoBateria == false)
        {
            ApagarLinterna();
        }
    }

    void PrenderLinterna()
    {
        luzLinterna.intensity = 100;
        linternaPrendida = true;
        StartCoroutine(temporizadorBateria);
    }

    void ApagarLinterna()
    {
        luzLinterna.intensity = 0;
        linternaPrendida = false;
        StopCoroutine(temporizadorBateria);
    }

    public IEnumerator DescontarBateria(float bateriaRestante = 100f)
    {
        bateria = bateriaRestante;

        while (bateriaRestante > 0)
        {
            textoBateria.text = "Bateria: " + bateriaRestante + "%";
            yield return new WaitForSeconds(3.0f);
            bateriaRestante--;
        }

        if (bateriaRestante == 0)
        {
            tengoBateria = false;
        }
    }
}
