using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    Vector2 MovMouse;
    Vector2 suavidad;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;
    GameObject jugador;

    void Start()
    {
        jugador = this.transform.parent.gameObject;
    }

    
    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));
        suavidad.x = Mathf.Lerp(suavidad.x, md.x, 1f / suavizado);
        suavidad.y = Mathf.Lerp(suavidad.y, md.y, 1f / suavizado);
        MovMouse += suavidad;
        MovMouse.y = Mathf.Clamp(MovMouse.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-MovMouse.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(MovMouse.x, jugador.transform.up);
    }
}
