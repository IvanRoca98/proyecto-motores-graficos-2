using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{
    public GameObject imagen;
    public GameObject texto1;
    public GameObject texto2;
    public GameObject texto3;
    public GameObject creditos;

    private void Awake()
    {
        imagen.SetActive(true);
        texto1.SetActive(true);
        texto2.SetActive(false);
        texto3.SetActive(false);
        creditos.SetActive(false);
    }

    public void PasarAlTexto2()
    {
        texto1.SetActive(false);
        texto2.SetActive(true);
    }

    public void PasarAlTexto3()
    {
        texto2.SetActive(false);
        texto3.SetActive(true);
    }

    public void Creditos()
    {
        imagen.SetActive(false);
        texto3.SetActive(false);
        creditos.SetActive(true);
    }

    public void VolverAlMenu()
    {
        SceneManager.LoadScene("Menu Principal");
    }
}
