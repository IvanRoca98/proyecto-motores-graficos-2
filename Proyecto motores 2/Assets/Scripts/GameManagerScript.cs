using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    #region Declaraciones
    public GameObject enemigo;
    public GameObject coleccionable;
    public GameObject jugador;
    public GameObject llave;
    public GameObject trigger;
    public Transform auto;
    public Transform cadaver;
    public Transform puerta;
    public Transform spawn;
    [HideInInspector] public bool enemigoInstanciado;
    [SerializeField] GameObject textoJuego;
    [SerializeField] GameObject menuPausa;
    public ControlJugador scriptJugador;
    public ControlCamara scriptCamara;
    public ControlLuz scriptLuz;
    bool juegoPausado;
    bool llaveObtenida;
    bool puertaAbierta;
    bool jugadorEnRango;
    bool rangoDePuerta;
    bool rangoDeCadaver;
    bool rangoAuto;
    bool puedoInstanciarEnemigo;
    public LayerMask capaJugador;
    public List<Light> listaLuces = new List<Light>();
    public TMPro.TMP_Text textoLlave;
    public TMPro.TMP_Text textoPuerta;
    public TMPro.TMP_Text textoAuto;
    public TMPro.TMP_Text textoCadaver;
    public TMPro.TMP_Text huir;
    public float rango;
    public Animator animPuerta;
    #endregion

    void Awake()
    {
        textoJuego.SetActive(true);
        menuPausa.SetActive(false);
        trigger.SetActive(false);
        Time.timeScale = 1;
        scriptCamara.enabled = true;
        scriptJugador.enabled = true;
        scriptLuz.enabled = true;
        juegoPausado = false;
    }

    void Start()
    {
        enemigoInstanciado = false;
        Cursor.lockState = CursorLockMode.Locked;
        llaveObtenida = false;
        puertaAbierta = false;
        puedoInstanciarEnemigo = false;
        scriptJugador = (ControlJugador)jugador.GetComponent(typeof(ControlJugador));
        textoLlave.text = "";
        textoPuerta.text = "";
        textoAuto.text = "";
        textoCadaver.text = "";
        huir.text = "";
        animPuerta = puerta.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
            juegoPausado = true;
        }

        if (scriptJugador.instanciarEnemigo && enemigoInstanciado == false)
        {
            Instantiate(enemigo, spawn.position, Quaternion.identity);
            enemigoInstanciado = true;
        }

        if (juegoPausado)
        {
            Time.timeScale = 0;
            scriptCamara.enabled = false;
            scriptJugador.enabled = false;
            scriptLuz.enabled = false;
            textoJuego.SetActive(false);
            menuPausa.SetActive(true);
            AkSoundEngine.PostEvent("Stop_caminar", jugador);
            AkSoundEngine.PostEvent("Stop_correr", jugador);
        }

        jugadorEnRango = Physics.CheckSphere(llave.transform.position, rango, capaJugador);
        rangoDePuerta = Physics.CheckSphere(puerta.transform.position, rango, capaJugador);
        rangoDeCadaver = Physics.CheckSphere(cadaver.transform.position, rango, capaJugador);
        rangoAuto = Physics.CheckSphere(auto.transform.position, rango, capaJugador);

        if (jugadorEnRango && llaveObtenida == false)
        {
            textoLlave.text = "Presiona E para agarrar la llave";
            if (Input.GetKeyDown(KeyCode.E))
            {
                textoLlave.text = "";
                llave.SetActive(false);
                llaveObtenida = true;
                AkSoundEngine.PostEvent("Play_sonido_llaves", gameObject);
            }
        }
        else if (jugadorEnRango == false)
        {
            textoLlave.text = "";
        }

        if (rangoDePuerta && llaveObtenida && puertaAbierta == false)
        {
            textoPuerta.text = "Presiona E para abrir la puerta";
            if (Input.GetKeyDown(KeyCode.E))
            {
                animPuerta.SetBool("tengollave", true);
                puertaAbierta = true;
                AkSoundEngine.PostEvent("Play_sonido_abrir_puerta", gameObject);
                textoPuerta.text = "";

                foreach (Light luz in listaLuces)
                {
                    luz.intensity = 0f;
                }

                AkSoundEngine.PostEvent("Stop_musica_ambiente", GameObject.Find("sonidos de ambiente"));
            }
        }
        else if (rangoDePuerta == false)
        {
            textoPuerta.text = "";
        }

        if(rangoDePuerta && llaveObtenida == false)
        {
            textoPuerta.text = "Necesitas una llave para abrir la puerta";
        }
        else if (rangoDePuerta == false)
        {
            textoPuerta.text = "";
        }

        if (rangoDeCadaver && puertaAbierta)
        {
            puedoInstanciarEnemigo = true;
            textoCadaver.text = "Esta muerto!! Tengo que salir de aqui";
        }

        if (rangoDeCadaver == false)
        {
            textoCadaver.text = "";
        }

        if (puedoInstanciarEnemigo)
        {
            trigger.SetActive(true);
        }

        if (rangoAuto && enemigoInstanciado == false)
        {
            textoAuto.text = "Debo investigar antes de irme";
        }

        if (rangoAuto && enemigoInstanciado)
        {
            textoAuto.text = "Presiona E para subir al auto";

            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.LoadScene("Creditos");
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (rangoAuto == false)
        {
            textoAuto.text = "";
        }

        if (enemigoInstanciado)
        {
            huir.text = "HUYE!!";
            trigger.SetActive(false);
        }
    }

    public void Continuar()
    {
        juegoPausado = false;
        Time.timeScale = 1;
        scriptCamara.enabled = true;
        scriptJugador.enabled = true;
        scriptLuz.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        textoJuego.SetActive(true);
        menuPausa.SetActive(false);
    }

    public void VolverAlMenu()
    {
        SceneManager.LoadScene("Menu Principal");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(llave.transform.position, rango);
        Gizmos.DrawWireSphere(puerta.transform.position, rango);
        Gizmos.DrawWireSphere(cadaver.transform.position, rango);
        Gizmos.DrawWireSphere(auto.transform.position, rango);
    }
}
