using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class ControlEnemigo : MonoBehaviour
{
    GameObject jugador;
    float rapidezMovimiento;
    public NavMeshAgent agente;
    private Animator animator;
    private bool stunned;
    private bool puedoReproducirSonido;

    void Start()
    {
        jugador = GameObject.Find("Jugador");
        rapidezMovimiento = 10f;
        animator = GetComponent<Animator>();
        puedoReproducirSonido = true;
    }

    void Update()
    {
        agente.speed = rapidezMovimiento;
        agente.SetDestination(jugador.transform.position);

        if (rapidezMovimiento != 0)
        {
            animator.SetBool("Stunned", false);
            stunned = false;
        }
        else
        {
            animator.SetBool("Stunned", true);
            stunned = true;
        }

        if (stunned && puedoReproducirSonido)
        {
            AkSoundEngine.PostEvent("Play_grito_monstruo", gameObject);
            puedoReproducirSonido = false;
        }
        else if (stunned == false)
        {
            puedoReproducirSonido = true;
        }
    }

    public void Detenerse()
    {
        rapidezMovimiento = 0f;
        AkSoundEngine.PostEvent("Stop_pasos_monstruo", gameObject);
    }

    public void Avanzar()
    {
        rapidezMovimiento = 10f;
        AkSoundEngine.PostEvent("Play_pasos_monstruo", gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") == true && stunned == false)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("Jumpscare");
            AkSoundEngine.PostEvent("Stop_pasos_monstruo", gameObject);
        }
    }
}
