using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] GameObject tituloPrincipal;
    [SerializeField] GameObject texto;
    [SerializeField] GameObject menuSalir;

    void Awake()
    {
        tituloPrincipal.SetActive(true);
        texto.SetActive(false);
        menuSalir.SetActive(false);
    }

    public void Jugar()
    {
        SceneManager.LoadScene("Juego");
    }

    public void MostrarHistoria()
    {
        tituloPrincipal.SetActive(false);
        texto.SetActive(true);
    }

    public void EstaSeguro()
    {
        tituloPrincipal.SetActive(false);
        texto.SetActive(false);
        menuSalir.SetActive(true);
    }

    public void VolverAlMenu()
    {
        tituloPrincipal.SetActive(true);
        texto.SetActive(false);
        menuSalir.SetActive(false);
    }

    public void Salir()
    {
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;  //Comentar esta linea para la build
    }
}
