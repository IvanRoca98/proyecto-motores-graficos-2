/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ENTRAR_EN_CASA = 2325318790U;
        static const AkUniqueID PLAY_CAMINAR = 3694613279U;
        static const AkUniqueID PLAY_CORRER = 932480601U;
        static const AkUniqueID PLAY_FIN_DE_JUEGO = 511500458U;
        static const AkUniqueID PLAY_GRITO_MONSTRUO = 3438540139U;
        static const AkUniqueID PLAY_JUMPSCARE = 3840943850U;
        static const AkUniqueID PLAY_MUSICA_AMBIENTE = 1078152554U;
        static const AkUniqueID PLAY_MUSICA_MENU = 4098647122U;
        static const AkUniqueID PLAY_PASOS_MONSTRUO = 2922091292U;
        static const AkUniqueID PLAY_PELIGRO = 3725410508U;
        static const AkUniqueID PLAY_SONIDO_ABRIR_PUERTA = 152196069U;
        static const AkUniqueID PLAY_SONIDO_AUTO = 2337950978U;
        static const AkUniqueID PLAY_SONIDO_LLAVES = 4071099954U;
        static const AkUniqueID SALIR_DE_CASA = 2659321951U;
        static const AkUniqueID STOP_CAMINAR = 2686021093U;
        static const AkUniqueID STOP_CORRER = 3807469107U;
        static const AkUniqueID STOP_FIN_DE_JUEGO = 3916159316U;
        static const AkUniqueID STOP_JUEGO = 365570646U;
        static const AkUniqueID STOP_JUMPSCARE = 1214182468U;
        static const AkUniqueID STOP_MUSICA_AMBIENTE = 3704511504U;
        static const AkUniqueID STOP_MUSICA_MENU = 2734701852U;
        static const AkUniqueID STOP_PASOS_MONSTRUO = 2123859086U;
        static const AkUniqueID STOP_PUERTA_ABIERTA = 3533786370U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace SUELOS
        {
            static const AkUniqueID GROUP = 2868892988U;

            namespace SWITCH
            {
                static const AkUniqueID MADERA = 2306462273U;
                static const AkUniqueID PASTO = 3143905964U;
            } // namespace SWITCH
        } // namespace SUELOS

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID JUEGO = 1157012013U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBISONICS = 2903397179U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
